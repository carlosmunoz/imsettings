# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-01-07 18:11+0900\n"
"PO-Revision-Date: \n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.0.3\n"

#: ../backends/xim/loopback.c:223
msgid "Synchronous"
msgstr ""

#: ../backends/xim/loopback.c:224
msgid "Request to send a key event synchronously"
msgstr ""

#: ../backends/xim/main.c:155
msgid "D-Bus address to use"
msgstr ""

#: ../backends/xim/main.c:155
msgid "ADDRESS"
msgstr ""

#: ../backends/xim/main.c:156
msgid "X display to use"
msgstr ""

#: ../backends/xim/main.c:156
msgid "DISPLAY"
msgstr ""

#: ../backends/xim/main.c:157
msgid "Replace the running XIM server with new instance."
msgstr ""

#: ../backends/xim/main.c:158
msgid "Output the debugging logs"
msgstr ""

#: ../backends/xim/main.c:159
msgid "XIM server connects to, for debugging purpose only"
msgstr ""

#: ../backends/xim/main.c:159
msgid "XIM"
msgstr ""

#: ../backends/xim/main.c:186 ../imsettings-daemon/main.c:139
#: ../utils/imsettings-check.c:313 ../utils/imsettings-info.c:69
#: ../utils/imsettings-reload.c:63 ../utils/imsettings-switch.c:80
msgid "Unknown error in parsing the command lines."
msgstr ""

#: ../backends/xim/proxy.c:2972
msgid "XIM server name"
msgstr ""

#: ../backends/xim/proxy.c:2973
msgid "The name of XIM server for the connection"
msgstr ""

#: ../backends/xim/proxy.c:2978
msgid "Signals for Protocol class"
msgstr ""

#: ../backends/xim/proxy.c:2979
msgid "A structure of signals for Protocol class"
msgstr ""

#: ../imsettings/imsettings-client.c:161
msgid "Locale"
msgstr ""

#: ../imsettings/imsettings-client.c:162
msgid "Locale to get the imsettings information"
msgstr ""

#: ../imsettings-daemon/imsettings-module.c:133
msgid "Name"
msgstr ""

#: ../imsettings-daemon/imsettings-module.c:134
msgid "A module name for imsettings backend"
msgstr ""

#: ../imsettings-daemon/imsettings-proc.c:193
msgid "Unable to keep Input Method running"
msgstr ""

#: ../imsettings-daemon/imsettings-proc.c:203
#, c-format
msgid ""
"Giving up to bring the process up because %s Input Method process for %s "
"rapidly died many times. See $XDG_CACHE_HOME/imsettings/log for more details."
msgstr ""

#: ../imsettings-daemon/imsettings-proc.c:304
#, c-format
msgid "[BUG] %s process is still running [pid: %d]\n"
msgstr ""

#: ../imsettings-daemon/imsettings-proc.c:448
#, c-format
msgid "Couldn't send a signal to the %s process successfully."
msgstr ""

#: ../imsettings-daemon/imsettings-proc.c:566
msgid "IMSettingsInfo"
msgstr ""

#: ../imsettings-daemon/imsettings-proc.c:567
msgid "A GObject to be a IMSettingsInfo"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:417
msgid "DBus connection"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:418
msgid "A GObject to be a DBus connection"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:423
msgid "Logging"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:424
msgid "A boolean value whether the logging facility is enabled or not."
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:429
#: ../imsettings-daemon/imsettings-server.c:430
msgid "Home directory"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:435
#: ../imsettings-daemon/imsettings-server.c:436
msgid "xinputrc directory"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:441
#: ../imsettings-daemon/imsettings-server.c:442
msgid "xinput directory"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:447
msgid "module directory"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:448
msgid "IMSettings module directory"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:725
#, c-format
msgid "No such input method on your system: %s"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:734
#: ../imsettings-daemon/imsettings-server.c:883
#: ../imsettings-daemon/imsettings-server.c:924
#: ../imsettings-daemon/imsettings-server.c:1136
#, c-format
msgid "Out of memory"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:759
#, c-format
msgid "Current desktop isn't targeted by IMSettings."
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:811
#, c-format
msgid "Failed to revert the backup file: %s"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:824
#, c-format
msgid "Failed to create a backup file: %s"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:834
#, c-format
msgid "Failed to remove an user xinputrc file: %s"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:845
#, c-format
msgid "Failed to create a symlink: %s"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:876
#, c-format
msgid "No system-wide xinputrc available"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:916
#, c-format
msgid "No user xinputrc nor system-wide xinputrc available"
msgstr ""

#: ../imsettings-daemon/imsettings-server.c:1128
#, c-format
msgid "No such input method: %s"
msgstr ""

#: ../imsettings-daemon/main.c:111
msgid "Replace the instance of the imsettings daemon."
msgstr ""

#: ../imsettings-daemon/main.c:112
msgid "Set the system-wide xinputrc directory (for debugging purpose)"
msgstr ""

#: ../imsettings-daemon/main.c:112 ../imsettings-daemon/main.c:113
#: ../imsettings-daemon/main.c:114 ../imsettings-daemon/main.c:115
msgid "DIR"
msgstr ""

#: ../imsettings-daemon/main.c:113
msgid "Set the IM configuration directory (for debugging purpose)"
msgstr ""

#: ../imsettings-daemon/main.c:114
msgid "Set a home directory (for debugging purpose)"
msgstr ""

#: ../imsettings-daemon/main.c:115
msgid "Set the imsettings module directory (for debugging purpose)"
msgstr ""

#: ../imsettings-daemon/main.c:116
msgid "Do not create a log file."
msgstr ""

#: ../utils/imsettings-check.c:131
#, c-format
msgid "Unable to open X display"
msgstr ""

#: ../utils/imsettings-check.c:138
#, c-format
msgid "XSETTINGS manager isn't running"
msgstr ""

#: ../utils/imsettings-check.c:169 ../utils/imsettings-check.c:222
#, c-format
msgid "Unable to create a client instance."
msgstr ""

#: ../utils/imsettings-check.c:177 ../utils/imsettings-check.c:230
#, c-format
msgid "imsettings version mismatch"
msgstr ""

#: ../utils/imsettings-check.c:190 ../utils/imsettings-check.c:256
#, c-format
msgid "No modules loaded"
msgstr ""

#: ../utils/imsettings-check.c:248
#, c-format
msgid "Please see .imsettings.log for more details"
msgstr ""

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-check.c:287
msgid "Output the detail information for the result"
msgstr ""

#: ../utils/imsettings-check.c:288
msgid "Check if DBus is running"
msgstr ""

#: ../utils/imsettings-check.c:289
msgid "Check if SESSION is on line"
msgstr ""

#: ../utils/imsettings-check.c:290
msgid "Check if any valid modules are loaded"
msgstr ""

#: ../utils/imsettings-check.c:291
msgid "Check if all of the module settings has consistencies"
msgstr ""

#: ../utils/imsettings-check.c:292
msgid "Check if XSETTINGS manager is running"
msgstr ""

#: ../utils/imsettings-info.c:51 ../utils/imsettings-switch.c:44
msgid "[Input Method name|xinput.conf]"
msgstr ""

#: ../utils/imsettings-info.c:75 ../utils/imsettings-list.c:62
#: ../utils/imsettings-reload.c:70 ../utils/imsettings-switch.c:86
#, c-format
msgid "IMSettings is disabled on the system.\n"
msgstr ""

#: ../utils/imsettings-info.c:81 ../utils/imsettings-list.c:68
#: ../utils/imsettings-reload.c:76 ../utils/imsettings-switch.c:92
#, c-format
msgid "Unable to create a client instance.\n"
msgstr ""

#: ../utils/imsettings-info.c:88 ../utils/imsettings-list.c:75
#: ../utils/imsettings-switch.c:99
#, c-format
msgid ""
"Currently a different version of imsettings is running.\n"
"Running \"imsettings-reload\" may help but it will restart the Input Method\n"
msgstr ""

#: ../utils/imsettings-info.c:106
#, c-format
msgid "Unable to obtain an Input Method Information: %s\n"
msgstr ""

#: ../utils/imsettings-reload.c:44
msgid "Force reloading imsettings-daemon (deprecated)."
msgstr ""

#: ../utils/imsettings-reload.c:108
#, c-format
msgid "Reloaded.\n"
msgstr ""

#: ../utils/imsettings-start.desktop.in.h:1
msgid "Input Method starter"
msgstr ""

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:47
msgid "Force restarting the IM process regardless of any errors."
msgstr ""

#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:49
msgid "Do not update the user xinputrc."
msgstr ""

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:51
msgid "Shut up the extra messages."
msgstr ""

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:53
msgid "Restart input method"
msgstr ""

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:55
msgid "Read xinputrc to determine the input method"
msgstr ""

#: ../utils/imsettings-switch.c:108
msgid "No backend modules available"
msgstr ""

#: ../utils/imsettings-switch.c:125
#, c-format
msgid "No Input Method running to be restarted.\n"
msgstr ""

#: ../utils/imsettings-switch.c:138
#, c-format
msgid "Restarted %s\n"
msgstr ""

#: ../utils/imsettings-switch.c:180
#, c-format
msgid "Switched input method to %s\n"
msgstr ""
