# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Akira  <akira+transifex@tagoh.org>, 2011.
# Ani Peter <apeter@redhat.com>, 2006, 2009.
# anipeter <anipeter@fedoraproject.org>, 2012. #zanata
# anipeter <anipeter@fedoraproject.org>, 2013. #zanata
msgid ""
msgstr ""
"Project-Id-Version: imsettings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-01-07 18:11+0900\n"
"PO-Revision-Date: 2013-04-04 10:49-0400\n"
"Last-Translator: anipeter <anipeter@fedoraproject.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.0.3\n"

#: ../backends/xim/loopback.c:223
msgid "Synchronous"
msgstr "സിന്‍ക്രൊണസ്"

#: ../backends/xim/loopback.c:224
msgid "Request to send a key event synchronously"
msgstr "ഒരു കീ ഇവന്റ് സിന്‍ക്രൊണസായി അയയ്ക്കുന്നതിനു് ആവശ്യപ്പെടുക"

#: ../backends/xim/main.c:155
msgid "D-Bus address to use"
msgstr "ഉപയോഗിയ്ക്കുവാനുള്ള ഡി-ബസ് വിലാസം"

#: ../backends/xim/main.c:155
msgid "ADDRESS"
msgstr "വിലാസം"

#: ../backends/xim/main.c:156
msgid "X display to use"
msgstr "ഉപയോഗിക്കുന്നതിനുള്ള എക്സ് ഡിസ്പ്ലെ"

#: ../backends/xim/main.c:156
msgid "DISPLAY"
msgstr "DISPLAY"

#: ../backends/xim/main.c:157
msgid "Replace the running XIM server with new instance."
msgstr "പ്രവര്‍ത്തനത്തിലുള്ള XIM സര്‍വര്‍ പുതിയ ഇന്‍സ്റ്റന്‍സ് ഉപയോഗിച്ച് മാറ്റുക."

#: ../backends/xim/main.c:158
msgid "Output the debugging logs"
msgstr "ഡീബഗ്ഗിങ് ലോഗുകള്‍ ഔട്ട്പുട്ട് നല്‍കുന്നു"

#: ../backends/xim/main.c:159
msgid "XIM server connects to, for debugging purpose only"
msgstr "XIM സര്‍വര്‍ കണക്ട് ചെയ്യുന്നതു്, ഡീബഗ്ഗിനു് മാത്രം"

#: ../backends/xim/main.c:159
msgid "XIM"
msgstr "XIM"

#: ../backends/xim/main.c:186 ../imsettings-daemon/main.c:139
#: ../utils/imsettings-check.c:313 ../utils/imsettings-info.c:69
#: ../utils/imsettings-reload.c:63 ../utils/imsettings-switch.c:80
msgid "Unknown error in parsing the command lines."
msgstr "കമാന്‍ഡ് ലൈനുകള്‍ പാഴ്സ് ചെയ്യുന്നതില്‍ അപരിചിതമായ പിശക്."

#: ../backends/xim/proxy.c:2972
msgid "XIM server name"
msgstr "XIM സര്‍വര്‍ നാമം"

#: ../backends/xim/proxy.c:2973
msgid "The name of XIM server for the connection"
msgstr "കണക്ഷനുള്ള XIM സര്‍വറിന്റെ പേരു്"

#: ../backends/xim/proxy.c:2978
msgid "Signals for Protocol class"
msgstr "പ്രോട്ടോക്കോള്‍ ക്ലാസ്സിനുള്ള സിഗ്നലുകള്‍"

#: ../backends/xim/proxy.c:2979
msgid "A structure of signals for Protocol class"
msgstr "പ്രോട്ടോക്കോള്‍ ക്ലാസ്സിനുള്ള സിഗ്നലുകളുടെ സ്ട്രക്ചര്‍"

#: ../imsettings/imsettings-client.c:161
msgid "Locale"
msgstr "ലൊക്കെയില്‍"

#: ../imsettings/imsettings-client.c:162
msgid "Locale to get the imsettings information"
msgstr "ഐഎം വിവരങ്ങള്‍ ലഭ്യമാകുന്നതിനുള്ള ലൊക്കെയില്‍"

#: ../imsettings-daemon/imsettings-module.c:133
msgid "Name"
msgstr "പേരു്"

#: ../imsettings-daemon/imsettings-module.c:134
msgid "A module name for imsettings backend"
msgstr "imsettings ബാക്കെന്‍ഡിനുള്ളൊരു ഘടകനാമം"

#: ../imsettings-daemon/imsettings-proc.c:193
msgid "Unable to keep Input Method running"
msgstr "ഇന്‍പുട്ട് രീതി പ്രവര്‍ത്തിപ്പിക്കുന്നതിനു് സാധ്യമല്ല"

#: ../imsettings-daemon/imsettings-proc.c:203
#, c-format
msgid ""
"Giving up to bring the process up because %s Input Method process for %s "
"rapidly died many times. See $XDG_CACHE_HOME/imsettings/log for more details."
msgstr ""
"%s ഇന്‍പുട്ട് മെഥേഡ് പ്രക്രിയ, %s-നുള്ളതു്, പല തവണ പ്രവര്‍ത്തിക്കാത്തതിനാല്‍, പ്രക്രിയ തിരികെ "
"കൊണ്ടുവരാന്‍ ഇനി ശ്രമിക്കുന്നില്ല. കൂടുതല്‍ വിവരങ്ങള്‍ക്കായി $XDG_CACHE_HOME/imsettings/log "
"കാണുക."

#: ../imsettings-daemon/imsettings-proc.c:304
#, c-format
msgid "[BUG] %s process is still running [pid: %d]\n"
msgstr "[BUG] %s പ്രക്രിയ ഇപ്പോഴും [pid: %d] പ്രവര്‍ത്തിപ്പിയ്ക്കുന്നു ¶\n"

#: ../imsettings-daemon/imsettings-proc.c:448
#, c-format
msgid "Couldn't send a signal to the %s process successfully."
msgstr "വിജയകരമായി %s പ്രക്രിയയിലേക്ക് ഒരു സിഗ്നല്‍ അയയ്ക്കുവാന്‍ സാധ്യമായില്ല."

#: ../imsettings-daemon/imsettings-proc.c:566
msgid "IMSettingsInfo"
msgstr "IMSettingsInfo"

#: ../imsettings-daemon/imsettings-proc.c:567
msgid "A GObject to be a IMSettingsInfo"
msgstr "IMSettingsInfo ആകുന്നതിനുള്ളൊരു GObject"

#: ../imsettings-daemon/imsettings-server.c:417
msgid "DBus connection"
msgstr "ഡിബസ് കണക്ഷന്‍"

#: ../imsettings-daemon/imsettings-server.c:418
msgid "A GObject to be a DBus connection"
msgstr "ഒരു ഡിബസ് കണക്ഷനിലേക്കുള്ളൊരു GObject"

#: ../imsettings-daemon/imsettings-server.c:423
msgid "Logging"
msgstr "പ്രവേശിയ്ക്കുന്നു"

#: ../imsettings-daemon/imsettings-server.c:424
msgid "A boolean value whether the logging facility is enabled or not."
msgstr "പ്രവേശന സൌകര്യം പ്രവര്‍ത്തന സജ്ജമാണോ എന്നറിയുന്നതിനായി ബൂളിയന്‍ മൂല്ല്യം."

#: ../imsettings-daemon/imsettings-server.c:429
#: ../imsettings-daemon/imsettings-server.c:430
msgid "Home directory"
msgstr "ഹോം ഡയറക്ടറി"

#: ../imsettings-daemon/imsettings-server.c:435
#: ../imsettings-daemon/imsettings-server.c:436
msgid "xinputrc directory"
msgstr "xinputrc ഡയറക്ടറി"

#: ../imsettings-daemon/imsettings-server.c:441
#: ../imsettings-daemon/imsettings-server.c:442
msgid "xinput directory"
msgstr "xinput ഡയറക്ടറി"

#: ../imsettings-daemon/imsettings-server.c:447
msgid "module directory"
msgstr "ഘടകത്തിനുള്ള ഡയറക്ടറി"

#: ../imsettings-daemon/imsettings-server.c:448
msgid "IMSettings module directory"
msgstr "IMSettings-നുള്ള ഘടക ഡയറക്ടറി"

#: ../imsettings-daemon/imsettings-server.c:725
#, c-format
msgid "No such input method on your system: %s"
msgstr "നിങ്ങളുടെ സിസ്റ്റമില്‍ അത്തരം ഒരു ഇന്‍പുട്ട് രീതിയില്ല: %s"

#: ../imsettings-daemon/imsettings-server.c:734
#: ../imsettings-daemon/imsettings-server.c:883
#: ../imsettings-daemon/imsettings-server.c:924
#: ../imsettings-daemon/imsettings-server.c:1136
#, c-format
msgid "Out of memory"
msgstr "ആവശ്യമായ മെമ്മറി ലഭ്യമല്ല"

#: ../imsettings-daemon/imsettings-server.c:759
#, c-format
msgid "Current desktop isn't targeted by IMSettings."
msgstr "നിലവിലുള്ള പണിയിടം IMSettings.-ല്‍ ലഭ്യമല്ല."

#: ../imsettings-daemon/imsettings-server.c:811
#, c-format
msgid "Failed to revert the backup file: %s"
msgstr "ബാക്കപ്പ് ഫയല്‍ തിരികെ ലഭ്യമാക്കുന്നതില്‍ പരാജയപ്പെട്ടു: %s"

#: ../imsettings-daemon/imsettings-server.c:824
#, c-format
msgid "Failed to create a backup file: %s"
msgstr "ഒരു ബാക്കപ്പ് ഫയല്‍ ഉണ്ടാക്കുന്നതില്‍ പരാജയപ്പെട്ടു: %s"

#: ../imsettings-daemon/imsettings-server.c:834
#, c-format
msgid "Failed to remove an user xinputrc file: %s"
msgstr "ഉപയോക്താവിനുള്ളൊരു xinputrc ഫയല്‍ നീക്കം ചെയ്യുന്നതില്‍ പരാജയം: %s"

#: ../imsettings-daemon/imsettings-server.c:845
#, c-format
msgid "Failed to create a symlink: %s"
msgstr "ഒരു സിംലിങ്ക് തയ്യാറാക്കുന്നതില്‍ പരാജയം: %s"

#: ../imsettings-daemon/imsettings-server.c:876
#, c-format
msgid "No system-wide xinputrc available"
msgstr "സിസ്റ്റത്തിനുടനീളമുള്ള xinputrc ലഭ്യമല്ല"

#: ../imsettings-daemon/imsettings-server.c:916
#, c-format
msgid "No user xinputrc nor system-wide xinputrc available"
msgstr "ഉപയോക്താവിനുള്ള xinputrc അല്ലെങ്കില്‍ സിസ്റ്റത്തിനു് പൂര്‍ണ്ണമായുള്ള xinputrc ലഭ്യമല്ല"

#: ../imsettings-daemon/imsettings-server.c:1128
#, c-format
msgid "No such input method: %s"
msgstr "അത്തരം ഇന്‍പുട്ട് രീതി ലഭ്യമല്ല: %s"

#: ../imsettings-daemon/main.c:111
msgid "Replace the instance of the imsettings daemon."
msgstr "imsettings ഡെമണിന്റെ ഇന്‍സ്റ്റന്‍സ് മാറ്റിസ്ഥാപിയ്ക്കുക."

#: ../imsettings-daemon/main.c:112
msgid "Set the system-wide xinputrc directory (for debugging purpose)"
msgstr "സിസ്റ്റത്തിനുടനീളമുള്ള xinputrc ഡയറക്ടറി സജ്ജമാക്കുക (ഡീബഗ്ഗിനായി)"

#: ../imsettings-daemon/main.c:112 ../imsettings-daemon/main.c:113
#: ../imsettings-daemon/main.c:114 ../imsettings-daemon/main.c:115
msgid "DIR"
msgstr "DIR"

#: ../imsettings-daemon/main.c:113
msgid "Set the IM configuration directory (for debugging purpose)"
msgstr "ഐഎം ക്രമീകരണ ഡയറക്ടറി സജ്ജമാക്കുക (ഡീബഗ്ഗിനായി)"

#: ../imsettings-daemon/main.c:114
msgid "Set a home directory (for debugging purpose)"
msgstr "ഒരു ആസ്ഥാന ഡയറക്ടറി സജ്ജമാക്കുക (ഡീബഗ്ഗിനായി)"

#: ../imsettings-daemon/main.c:115
msgid "Set the imsettings module directory (for debugging purpose)"
msgstr "imsettings ഘടക ഡയറക്ടറി സജ്ജമാക്കുക (ഡീബഗ്ഗിനായി)"

#: ../imsettings-daemon/main.c:116
msgid "Do not create a log file."
msgstr "ഒരു ലോഗ് ഫയല്‍ ഉണ്ടാക്കേണ്ട."

#: ../utils/imsettings-check.c:131
#, c-format
msgid "Unable to open X display"
msgstr "എക്സ് പ്രദര്‍ശനം തുറക്കുവാന്‍ സാധ്യമല്ല"

#: ../utils/imsettings-check.c:138
#, c-format
msgid "XSETTINGS manager isn't running"
msgstr "XSETTINGS മാനേജര്‍ പ്രവര്‍ത്തിയ്ക്കുന്നില്ല"

#: ../utils/imsettings-check.c:169 ../utils/imsettings-check.c:222
#, c-format
msgid "Unable to create a client instance."
msgstr "ഒരു ക്ലയന്റ് ഇന്‍സ്റ്റന്‍സ് തയ്യാറാക്കുവാന്‍ സാധ്യമല്ല."

#: ../utils/imsettings-check.c:177 ../utils/imsettings-check.c:230
#, c-format
msgid "imsettings version mismatch"
msgstr "imsettings പതിപ്പു് ചേരുന്നില്ല"

#: ../utils/imsettings-check.c:190 ../utils/imsettings-check.c:256
#, c-format
msgid "No modules loaded"
msgstr "ഘടകങ്ങള്‍ ലഭ്യമാക്കുവാന്‍ സാധ്യമല്ല"

#: ../utils/imsettings-check.c:248
#, c-format
msgid "Please see .imsettings.log for more details"
msgstr "കൂടുതല്‍ വിവരങ്ങള്‍ക്കായി .imsettings.log ദയവായി കാണുക"

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-check.c:287
msgid "Output the detail information for the result"
msgstr "ഫലത്തിനുള്ള വിശദമായ വിവരം ലഭ്യമാക്കുക"

#: ../utils/imsettings-check.c:288
msgid "Check if DBus is running"
msgstr "ഡിബസ് പ്രവര്‍ത്തിക്കുന്നുവോ എന്നു് പരിശോധിയ്ക്കുക"

#: ../utils/imsettings-check.c:289
msgid "Check if SESSION is on line"
msgstr "സെഷന്‍ ഓണ്‍ലൈന്‍ ആണോ എന്നു് പരിശോധിയ്ക്കുക"

#: ../utils/imsettings-check.c:290
msgid "Check if any valid modules are loaded"
msgstr "ശരിയായ ഘടകങ്ങള്‍ ലഭ്യമോ എന്നു് പരിശോധിയ്ക്കുക"

#: ../utils/imsettings-check.c:291
msgid "Check if all of the module settings has consistencies"
msgstr "എല്ലാ ഘടക സജ്ജീകരണങ്ങള്‍ക്കും സ്ഥിരതയുണ്ടോ എന്നു് പരിശോധിയ്ക്കുക"

#: ../utils/imsettings-check.c:292
msgid "Check if XSETTINGS manager is running"
msgstr "XSETTINGS മാനേജര്‍ പ്രവര്‍ത്തിയ്ക്കുന്നോ എന്നു് പരിശോധിയ്ക്കുക"

#: ../utils/imsettings-info.c:51 ../utils/imsettings-switch.c:44
msgid "[Input Method name|xinput.conf]"
msgstr "[Input Method name|xinput.conf]"

#: ../utils/imsettings-info.c:75 ../utils/imsettings-list.c:62
#: ../utils/imsettings-reload.c:70 ../utils/imsettings-switch.c:86
#, c-format
msgid "IMSettings is disabled on the system.\n"
msgstr "IMSettings സിസ്റ്റത്തില്‍ പ്രവര്‍ത്തന രഹിതം.\n"

#: ../utils/imsettings-info.c:81 ../utils/imsettings-list.c:68
#: ../utils/imsettings-reload.c:76 ../utils/imsettings-switch.c:92
#, c-format
msgid "Unable to create a client instance.\n"
msgstr "ക്ലയന്റിനുള്ളൊരു ഇന്‍സ്റ്റന്‍സ് തയ്യാറാക്കുവാന്‍ സാധ്യമല്ല.\n"

#: ../utils/imsettings-info.c:88 ../utils/imsettings-list.c:75
#: ../utils/imsettings-switch.c:99
#, c-format
msgid ""
"Currently a different version of imsettings is running.\n"
"Running \"imsettings-reload\" may help but it will restart the Input Method\n"
msgstr ""
"നിലവില്‍ imsettings-ന്റെ മറ്റൊരു പതിപ്പു് പ്രവര്‍ത്തിയ്ക്കുന്നു.¶\n"
"\"imsettings-reload\" പ്രവര്‍ത്തിപ്പിയ്ക്കുന്നതു് ഒരു പക്ഷേ സഹായകമാണു്, പക്ഷേ ഇന്‍പുട്ട് രീതി "
"വീണ്ടും ആരംഭിക്കേണ്ടതായുണ്ടു്¶\n"

#: ../utils/imsettings-info.c:106
#, c-format
msgid "Unable to obtain an Input Method Information: %s\n"
msgstr "ഒരു ഇന്‍പുട്ട് രീതിയ്ക്കുള്ള വിവരം ലഭ്യമാക്കുവാന്‍ സാധ്യമല്ല: %s¶\n"

#: ../utils/imsettings-reload.c:44
msgid "Force reloading imsettings-daemon (deprecated)."
msgstr "imsettings-daemon വീണ്ടും ലഭ്യമാക്കുന്നതിനു് നിര്‍ബന്ധിയ്ക്കുക (ഇല്ലാതാക്കിയിരിയ്ക്കുന്നു)"

#: ../utils/imsettings-reload.c:108
#, c-format
msgid "Reloaded.\n"
msgstr "വീണ്ടും ലഭ്യമാക്കിയിരിയ്ക്കുന്നു.¶\n"

#: ../utils/imsettings-start.desktop.in.h:1
msgid "Input Method starter"
msgstr "ഇന്‍പുട്ട് മെഥേഡ് ആരംഭിക്കുന്നതിനുള്ള സംവിധാനം"

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:47
msgid "Force restarting the IM process regardless of any errors."
msgstr "പിശകുകളുണ്ടേലും ഐഎം പ്രക്രിയ നിര്‍ബന്ധമായും വീണ്ടും ആരംഭിക്കുക."

#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:49
msgid "Do not update the user xinputrc."
msgstr "ഉപയോക്താവിനുള്ള xinputrc പരിഷ്കരിയ്ക്കേണ്ടതില്ല."

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:51
msgid "Shut up the extra messages."
msgstr "എക്സ്ട്രാ സന്ദേശങ്ങള്‍ അടച്ചുപൂട്ടുക."

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:53
msgid "Restart input method"
msgstr "ഇന്‍പുട്ട് രീതി വീണ്ടും ആരംഭിയ്ക്കുക"

# For translators: this is a translation for the command-line option.
#. For translators: this is a translation for the command-line option.
#: ../utils/imsettings-switch.c:55
msgid "Read xinputrc to determine the input method"
msgstr "ഇന്‍പുട്ട് രീതി കണ്ടുപിടിയ്ക്കുന്നതിനായി xinputrc ലഭ്യമാക്കുക"

#: ../utils/imsettings-switch.c:108
msgid "No backend modules available"
msgstr "ബാക്കന്‍ഡ് ഘടകങ്ങള്‍ ലഭ്യമല്ല"

#: ../utils/imsettings-switch.c:125
#, c-format
msgid "No Input Method running to be restarted.\n"
msgstr "പ്രവര്‍ത്തനത്തിലുള്ള ഒരു ഇന്‍പുട്ട് രീതിയും പ്രവര്‍ത്തിപ്പിയ്ക്കേണ്ടതില്ല.¶\n"

#: ../utils/imsettings-switch.c:138
#, c-format
msgid "Restarted %s\n"
msgstr "%s വീണ്ടും ആരംഭിച്ചിരിയ്ക്കുന്നു¶\n"

#: ../utils/imsettings-switch.c:180
#, c-format
msgid "Switched input method to %s\n"
msgstr "നിവേശക രീതി %s-ലേക്കു് മാറ്റുക\n"
